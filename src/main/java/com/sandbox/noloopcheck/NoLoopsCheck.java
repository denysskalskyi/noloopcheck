package com.sandbox.noloopcheck;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class NoLoopsCheck
{
    private final List<List<String>> _paths = new ArrayList<>();

    private Boolean doesItConnectTailWithHeadOfPath(final String start,
                                                    final String finish)
    {
        return _paths.stream()
                     .filter(path -> start.equals(path.get(path.size() - 1)) && finish.equals(path.get(0)))
                     .findFirst()
                     .isPresent();
    }

    private Boolean isItTailOfPath(final String start)
    {
        return _paths.stream()
                     .filter(path -> start.equals(path.get(path.size() - 1)))
                     .findFirst()
                     .isPresent();
    }

    private Boolean isItHeadOfPath(final String finish)
    {
        return _paths.stream()
                     .filter(path -> finish.equals(path.get(0)))
                     .findFirst()
                     .isPresent();
    }

    private Boolean hasLoop(final String start,
                            final String finish)
    {
        return start.equals(finish)
               || doesItConnectTailWithHeadOfPath(start, finish)
               || (isItTailOfPath(start) && isItHeadOfPath(finish));
    }

    public Boolean process(NoLoopsCandidate candidate)
    {
        for (final String start : candidate.getProvides())
        {
            for (final String finish : candidate.getRequires())
            {
                if (hasLoop(start, finish))
                {
                    return Boolean.FALSE;
                }
                else
                {
                    final List<List<String>> filteredPaths = _paths.stream()
                                                                   .filter(p -> start.equals(p.get(p.size() - 1)))
                                                                   .collect(Collectors.toList());
                    if (filteredPaths.isEmpty())
                    {
                        final List<String> path = new ArrayList<>();
                        path.add(start);
                        path.add(finish);
                        _paths.add(path);
                    }
                    else
                    {
                        filteredPaths.forEach(p -> p.add(finish));
                    }
                }
            }
        }
        return Boolean.TRUE;
    }
}
