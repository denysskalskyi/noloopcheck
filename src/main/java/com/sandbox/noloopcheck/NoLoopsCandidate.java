package com.sandbox.noloopcheck;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class NoLoopsCandidate
{
    private final Set<String> _provides = new HashSet<>();
    private final Set<String> _requires = new HashSet<>();

    public NoLoopsCandidate(final List<String> provides,
                            final List<String> requires)
    {
        _provides.addAll(provides);
        _requires.addAll(requires);
    }

    public Set<String> getProvides()
    {
        return _provides;
    }

    public Set<String> getRequires()
    {
        return _requires;
    }
}
