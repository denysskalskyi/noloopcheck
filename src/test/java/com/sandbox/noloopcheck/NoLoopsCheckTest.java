package com.sandbox.noloopcheck;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

public final class NoLoopsCheckTest
{
    @DataProvider
    public Object[][] loopsProvider()
    {
        return new Object[][] {
                
                // no loop
                { "b2935343-70ad-49b5-9c25-0338b1d314fd", 
                        Arrays.asList(new NoLoopsCandidate(Arrays.asList("A", "B"), Arrays.asList("C")), 
                                new NoLoopsCandidate(Arrays.asList("C"), Arrays.asList("D")),
                                new NoLoopsCandidate(Arrays.asList("D"), Arrays.asList("F"))),
                        Boolean.TRUE },
                
                // auto loop
                { "dd9e5793-4ccb-462c-95b9-f36084dcb483",
                        Arrays.asList(new NoLoopsCandidate(Arrays.asList("A", "B"), Arrays.asList("A"))),
                        Boolean.FALSE},
                
                // tail connects with head
                { "fd877cd7-2382-453e-8a66-eea3ca23c643",
                        Arrays.asList(new NoLoopsCandidate(Arrays.asList("A", "B"), Arrays.asList("C")),
                                new NoLoopsCandidate(Arrays.asList("C"), Arrays.asList("D")),
                                new NoLoopsCandidate(Arrays.asList("D"), Arrays.asList("A"))),
                        Boolean.FALSE},

                // connection in the middle
                {"1faa5f59-b5b1-40c6-af24-5a4200038599",
                        Arrays.asList(new NoLoopsCandidate(Arrays.asList("A", "B"), Arrays.asList("C")),
                                new NoLoopsCandidate(Arrays.asList("D"), Arrays.asList("A")),
                                new NoLoopsCandidate(Arrays.asList("C"), Arrays.asList("D"))),
                        Boolean.FALSE
                }
        };
    }

    @Test(dataProvider = "loopsProvider")
    public void loopsTest(final String testid,
                          final List<NoLoopsCandidate> candidateList,
                          final Boolean expectedResult)
    {
        final NoLoopsCheck check = new NoLoopsCheck();
        Boolean actualResult = Boolean.TRUE;
        for (final NoLoopsCandidate candidate : candidateList)
        {
            if (!check.process(candidate))
            {
                actualResult = Boolean.FALSE;
                break;
            }
        }
        Assert.assertEquals(actualResult, expectedResult, testid);
    }
}
